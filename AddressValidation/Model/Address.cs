﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MedJames.PersonalLines.AddressValidation.Interface;

namespace MedJames.PersonalLines.AddressValidation
{
    [ComVisible(true),
    GuidAttribute("caecb792-0175-4334-80c0-8ca1d47a633b")]
    [ProgId("AddressValidation.Address")]
    [ClassInterface(ClassInterfaceType.None)]
    public class Address : IAddress
    {
        internal string _addressLine1;
        internal string _addressLine2;
        internal string _city;
        internal string _state;
        internal string _zip;
        internal string _zipPlus4;
        internal string _country;

        public string AddressLine1
        { 
            get { return _addressLine1; } 
        }
        public string AddressLine2
        { 
            get { return _addressLine2; } 
        }
        public string City
        { 
            get { return _city; } 
        }
        public string State
        { 
            get { return _state; } 
        }
        public string Zip
        { 
            get { return _zip; } 
        }
        public string ZipPlus4
        { 
            get { return _zipPlus4; } 
        }
        public string Country
        { 
            get { return _country; } 
        }
    
    }
}
