﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MedJames.PersonalLines.AddressValidation.Interface;

namespace MedJames.PersonalLines.AddressValidation
{
    [ComVisible(true),
    GuidAttribute("ee25bdf6-877a-44b2-b568-7def65a900e8")]
    [ProgId("AddressValidation.AddressValidateResponse")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AddressValidateResponse : IAddressValidateResponse
    {
        internal string _status;
        internal string _statusCode;
        internal string _statusDescription;
        internal string _blockAddress;
        internal Address _address;

        public string Status
        {
            get { return _status; }
        }

        public string StatusCode
        {
            get { return _statusCode; }
        }

        public string StatusDescription
        {
            get { return _statusDescription; }
        }

        public string BlockAddress
        {
            get { return _blockAddress; }
        }

        public Address Address
        {
            get { return _address; }
        }
    }
}
