﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedJames.PersonalLines.AddressValidation.Interface
{
    [ComVisible(true),
    GuidAttribute("4d166290-2176-44ef-81b0-a7d6bac1119d")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IAddress
    {
        string AddressLine1 { get; }
        string AddressLine2 { get; }
        string City { get; }       
        string State { get; }       
        string Zip { get; }       
        string ZipPlus4 { get; }       
        string Country { get; }       
    }
}
