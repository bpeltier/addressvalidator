﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MedJames.PersonalLines.AddressValidation
{
    [ComVisible(true),
    GuidAttribute("f7126156-8c6f-4be8-bc5b-429cf4a9c012")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IValidateAddress
    {
        AddressValidateResponse ValidatedAddress { get; }

//        void ValidateMailingAddressRest(string Addr1, string Addr2, string City, string State, string Zip);
        void SetAddress(string Addr1, string Addr2, string City, string State, string Zip);

        void ValidateAddress();
    }
}
