﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedJames.PersonalLines.AddressValidation.Interface
{
    [ComVisible(true),
    GuidAttribute("6ef846ce-f56d-4aeb-9cb4-beea282f4e80")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IAddressValidateResponse
    {
        string Status { get; }
        string StatusCode { get; }
        string StatusDescription { get; }
        string BlockAddress { get; }
        Address Address { get; }
    }
}
