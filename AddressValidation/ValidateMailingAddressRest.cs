﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.Services.Protocols;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;
using System.Runtime.Serialization.Json;
using System.Runtime.InteropServices;

namespace MedJames.PersonalLines.AddressValidation
{
    [ComVisible(true),
    GuidAttribute("48c9b3dc-aa72-4171-99b2-4f526eae44c5")]
    [ProgId("AddressValidation.ValidateMailingAddressRest")]
    [ClassInterface(ClassInterfaceType.None)]
    public class ValidateMailingAddressRest : IValidateAddress
    {
        //private string VALIDATE_MAILINGADDRESS_OAUTH2_TOKEN_URL = "https://api.pitneybowes.com/oauth/token";
        //private string ValidateMailingAddressRest_API_URL = "https://api.pitneybowes.com/identify/identifyaddress/v1/rest/validatemailingaddress/";
        private string VALIDATE_MAILINGADDRESS_OAUTH2_TOKEN_URL = "https://api.precisely.com/oauth/token";
        private string ValidateMailingAddressRest_API_URL = "https://api.precisely.com/addressverification/v1/validatemailingaddress/";
        private string VALIDATE_MAILINGADDRESS_API_KEY = "LfPpSjkkA3hQ2SOWwBatbAbuSInISEJP";
        private string VALIDATE_MAILINGADDRESS_SECRET = "01n6yBD8PClIjFuu";
        private string VALIDATE_MAILINGADDRESS_AUTH_HEADER = "Authorization";
        private string accessToken;
        private string VALIDATE_MAILINGADDRESS_BEARER = "Bearer ";
        private string VALIDATE_MAILINGADDRESS_API_FRAGMENT_XML = "results.xml";
        private string API_FRAGMENT_JSON = "results.json";
        private string VALIDATE_MAILINGADDRESS_API = "http://www.pb.com/spectrum/services/ValidateMailingAddressAPI";
        private string VALIDATE_MAILINGADDRESS_API_NAMESPACE = "//a:xml.ValidateMailingAddressAPIResponse/a:Output/a:Row";
        private string VALIDATE_MAILINGADDRESS_API_AUTHTOKEN_CONTENTTYPE = "application/x-www-form-urlencoded";
        private string VALIDATE_MAILINGADDRESS_API_AUTHTOKEN_POSTDATA = "grant_type=client_credentials";
        private string VALIDATE_MAILINGADDRESS_API_HEADER_CONTENTTYPE = "application/xml;charset=utf-8";
        private string endPoint;

        private Address addressToValidate;
        private AddressValidateResponse validatedResponse;

        public AddressValidateResponse ValidatedAddress
        {
            get { return (validatedResponse); }
        }

        public ValidateMailingAddressRest()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            endPoint =  ValidateMailingAddressRest_API_URL + VALIDATE_MAILINGADDRESS_API_FRAGMENT_XML;
            acquireAuthToken();
        }

        public void SetAddress(string Addr1, string Addr2, string City, string State, string Zip)
        {
            this.addressToValidate = new Address();

            this.addressToValidate._addressLine1 = Addr1;
            this.addressToValidate._addressLine2 = Addr2;
            this.addressToValidate._city = City;
            this.addressToValidate._state = State;
            this.addressToValidate._zip = Zip;
            this.addressToValidate._country = "US";
        }

        public void ValidateAddress()
        {
            string inputstr = CreateRequestInput(this.addressToValidate);
            string result = processRequest(inputstr);
            this.validatedResponse = InflateResponse(result);
        }

        private string CreateRequestInput(Address pa)
        {
            string xmlString = "<ValidateMailingAddressAPIRequest>"
            + "<Input>"
            + "<Row>"
            + "<PostalCode>" + pa.Zip + "</PostalCode>"
            + "<StateProvince>" + pa.State + "</StateProvince>"
            + "<AddressLine1>" + pa.AddressLine1 + "</AddressLine1>"
            + "<AddressLine2>" + pa.AddressLine2 + "</AddressLine2>"
            + "<City>" + pa.City + "</City>"
            + "<Country>US</Country>"
            + "</Row>"
            + "</Input>"
            + "</ValidateMailingAddressAPIRequest>";

            return xmlString;
        }

        private AddressValidateResponse InflateResponse(string xmlstr)
        {
            AddressValidateResponse resp = new AddressValidateResponse();
            Address a = new Address();
            XmlDocument xml = new XmlDocument();
            var nsmgr = new XmlNamespaceManager(xml.NameTable);

            nsmgr.AddNamespace("a", VALIDATE_MAILINGADDRESS_API);
            xml.LoadXml(xmlstr);

            XmlNodeList xnList = xml.SelectNodes(VALIDATE_MAILINGADDRESS_API_NAMESPACE, nsmgr);
            foreach (XmlNode xn in xnList)
            {
                a._addressLine2 = (xn["ÄddressLine2"] == null) ? "" : xn["AddressLine2"].InnerText;
                a._addressLine1 = xn["AddressLine1"].InnerText;
                a._city = xn["City"].InnerText;
                a._state = xn["StateProvince"].InnerText;
                a._zip = xn["PostalCode.Base"].InnerText;
                a._country = "US";
                resp._status = (xn["Status"] == null) ? "" : xn["Status"].InnerText;
                resp._statusCode = (xn["Status.Code"] == null) ? "" : xn["Status.Code"].InnerText;
                resp._statusDescription = (xn["Status.Description"] == null) ? "" : xn["Status.Description"].InnerText;
                resp._blockAddress = xn["BlockAddress"].InnerText;
            }
            resp._address = a;
            return resp;
        }

        private void acquireAuthToken()
        {
            AdmAccessToken token = null;
            var request = (HttpWebRequest)WebRequest.Create(VALIDATE_MAILINGADDRESS_OAUTH2_TOKEN_URL);
            String PostData = string.Format(VALIDATE_MAILINGADDRESS_API_AUTHTOKEN_POSTDATA);
            String ContentType = VALIDATE_MAILINGADDRESS_API_AUTHTOKEN_CONTENTTYPE;
            request.Method = "POST";
            request.ContentLength = 0;
            request.ContentType = ContentType;

            var encoding = new UTF8Encoding();
            var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
            request.ContentLength = bytes.Length;

            String AuthorizationHeaderValue = "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(VALIDATE_MAILINGADDRESS_API_KEY + ":" + VALIDATE_MAILINGADDRESS_SECRET));

            WebHeaderCollection headers = new WebHeaderCollection();

            headers.Add(HttpRequestHeader.Authorization, AuthorizationHeaderValue);
            request.Headers = headers;

            try
            {
                using (var writeStream = request.GetRequestStream())
                {
                    writeStream.Write(bytes, 0, bytes.Length);
                }

                using (WebResponse webResponse = request.GetResponse())
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AdmAccessToken));
                    //Get deserialized object from JSON stream
                    token = (AdmAccessToken)serializer.ReadObject(webResponse.GetResponseStream());
                    accessToken = VALIDATE_MAILINGADDRESS_BEARER + token.access_token;
                }
            }
            catch (Exception e)
            {
                showException(e);
            }
        }

        private void showException(Exception e)
        {
            Console.WriteLine("Error Details : ");
            Console.WriteLine("Error Description : " + e.Message);

            if (e.InnerException != null)
                Console.WriteLine("Inner Exception : " + e.InnerException.Message);
        }

        private string processRequest(string inputString)
        {
            string result = "";
            string contentTypeString = VALIDATE_MAILINGADDRESS_API_HEADER_CONTENTTYPE;

            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, contentTypeString);
                client.Headers.Add(VALIDATE_MAILINGADDRESS_AUTH_HEADER, accessToken);
                result = client.UploadString(new Uri(endPoint), "POST", inputString);

                return result;
            }
        }

        [DataContract]
        private class AdmAccessToken
        {
            [DataMember]
            public string access_token { get; set; }
            [DataMember]
            public string token_type { get; set; }
            [DataMember]
            public string expires_in { get; set; }
            [DataMember]
            public string scope { get; set; }
        }
    }

}
