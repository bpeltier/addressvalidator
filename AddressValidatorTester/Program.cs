﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using MedJames.PersonalLines.AddressValidation;
/**
 * Code sample for accessing ValidateMailingAddress API. This program demonstrates the capability of ValidateMailingAddress API 
 * to Validate the addresses by consuming rest endpoint and sending XML and JSON input
 * To make it work, make sure to update your API_Key and SECRET before running this code. 
 */

namespace MedJames.PersonalLines.AddressValidatorTester
{
    class Program
    {
        public static void Main(String[] args)
        {
            var vmar = new ValidateMailingAddressRest();

////            vmar.SetAddress("3625 S Decatur street", "Apt 2037", "Las Vegas", "NV", "89103");
////            vmar.SetAddress("1350 N Town Center Dr Unit 1106", "", "Las Vegas", "NV", "89144");
            Console.WriteLine("1350 N Town Center Drive Unit 1106");
            Console.WriteLine("");
            Console.WriteLine("Las Vegas, NV  89144");
            vmar.SetAddress("1350 N Town Center Drive Unit 1106", "", "Las Vegas", "NV", "89144");
            vmar.ValidateAddress();

            Console.WriteLine();
            Console.WriteLine(string.Format("status:{0} - statuscode:{1}", vmar.ValidatedAddress.Status, vmar.ValidatedAddress.StatusCode));
            Console.WriteLine(string.Format("status description: {0}", vmar.ValidatedAddress.StatusDescription));
            Console.WriteLine();

            Console.WriteLine(vmar.ValidatedAddress.BlockAddress);
            Console.WriteLine();

            var retadd = vmar.ValidatedAddress.Address;
            Console.WriteLine(retadd.AddressLine1);
            Console.WriteLine(retadd.AddressLine2);
            Console.WriteLine(string.Format("{0}, {1}  {2}", retadd.City, retadd.State, retadd.Zip));

            Console.ReadKey();
        }
    }
}




