VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4155
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   ScaleHeight     =   4155
   ScaleWidth      =   5655
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtStatus 
      Height          =   495
      Left            =   2640
      MultiLine       =   -1  'True
      TabIndex        =   9
      Top             =   1500
      Width           =   2355
   End
   Begin VB.TextBox txtZipV 
      Height          =   315
      Left            =   3720
      MaxLength       =   10
      TabIndex        =   8
      Text            =   "Text4"
      Top             =   2700
      Width           =   1215
   End
   Begin VB.TextBox txtStateV 
      Height          =   345
      Left            =   3120
      MaxLength       =   2
      TabIndex        =   7
      Text            =   "Text3"
      Top             =   2700
      Width           =   510
   End
   Begin VB.TextBox txtCityV 
      Height          =   375
      Left            =   360
      TabIndex        =   6
      Text            =   "Text2"
      Top             =   2700
      Width           =   2535
   End
   Begin VB.TextBox txtAddressV 
      Height          =   315
      Left            =   360
      TabIndex        =   5
      Text            =   "Text1"
      Top             =   2280
      Width           =   4575
   End
   Begin VB.CommandButton cmdValidate 
      Caption         =   "Validate"
      Height          =   495
      Left            =   360
      TabIndex        =   4
      Top             =   1500
      Width           =   2175
   End
   Begin VB.TextBox txtZip 
      Height          =   315
      Left            =   3720
      MaxLength       =   10
      TabIndex        =   3
      Text            =   "Text4"
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtState 
      Height          =   345
      Left            =   3120
      MaxLength       =   2
      TabIndex        =   2
      Text            =   "Text3"
      Top             =   840
      Width           =   510
   End
   Begin VB.TextBox txtCity 
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Text            =   "Text2"
      Top             =   840
      Width           =   2535
   End
   Begin VB.TextBox txtAddress1 
      Height          =   315
      Left            =   360
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   420
      Width           =   4575
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'BCP Run this in the personallines install folder so that all the components are found and can load

Private Sub cmdValidate_Click()
    Dim av As MedJames_PersonalLines_AddressValidation.ValidateMailingAddressRest
    Set av = New MedJames_PersonalLines_AddressValidation.ValidateMailingAddressRest
    
    Call av.SetAddress(Me.txtAddress1.Text, "", Me.txtCity.Text, Me.txtState.Text, Me.txtZip.Text)
    Call av.ValidateAddress
    
    If av.ValidatedAddress.Status <> "" Then
        Me.txtStatus.Text = av.ValidatedAddress.Status + ":" + av.ValidatedAddress.StatusCode + vbCrLf + _
            av.ValidatedAddress.StatusDescription
    Else
        Me.txtStatus.Text = ""
    End If
                
    Me.txtAddressV.Text = av.ValidatedAddress.Address.AddressLine1
    Me.txtCityV.Text = av.ValidatedAddress.Address.City
    Me.txtStateV.Text = av.ValidatedAddress.Address.State
    Me.txtZipV.Text = av.ValidatedAddress.Address.Zip
    
    If Len(Me.txtStatus.Text) > 0 Then
        Call IsGoodAddress(False)
    Else
        Call IsGoodAddress(True)
    End If
    
    
'    Dim adlib As AddressValidatorLib.AddressValidator
'    Set adlib = New AddressValidatorLib.AddressValidator
'    MsgBox "helloworld = " & adlib.HelloWorld
    
End Sub

Private Sub IsGoodAddress(ByVal b As Boolean)
    If b = False Then
        Me.txtAddressV.BackColor = vbRed
        Me.txtCityV.BackColor = vbRed
        Me.txtStateV.BackColor = vbRed
        Me.txtZipV.BackColor = vbRed
    Else
        Me.txtAddressV.BackColor = vbWhite
        Me.txtCityV.BackColor = vbWhite
        Me.txtStateV.BackColor = vbWhite
        Me.txtZipV.BackColor = vbWhite
    End If
End Sub

Private Sub Form_Load()

    Me.txtAddress1.Text = "8595 college blvd"
    Me.txtCity.Text = "Overland Par"
    Me.txtState.Text = "KS"
    Me.txtZip.Text = "66212"
    Me.txtAddressV = ""
    Me.txtCityV = ""
    Me.txtStateV = ""
    Me.txtZipV = ""
    
End Sub
